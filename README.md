# Prediction Model for Diabetes using Deep Learning
# By- Aarush Kumar
Purpose: International Diabetes Federation (IDF) stated that 382 million people are living with diabetes worldwide. Over the last few years, the impact of diabetes has been increased drastically, which makes it a global threat. At present, Diabetes has steadily been listed in the top position as a major cause of death. The number of affected people will reach up to 629 million i.e. 48% increase by 2045. However, diabetes is largely preventable and can be avoided by making lifestyle changes. These changes can also lower the chances of developing heart disease and cancer. So, there is a dire need for a prognosis tool that can help the doctors with early detection of the disease and hence can recommend the lifestyle changes required to stop the progression of the deadly disease. 
Model:The accuracy achieved by functional classifiers Logistic Regression,Decision Tree (DT) and Deep Learning (DL) lies within the range of 70-80% for the used dataset. Among the three of them, Logistic Regression provides the best results for diabetes onset with an accuracy rate of 79.00%. Hence, this proposed system provides an effective prognostic tool for healthcare officials. The results obtained can be used to develop a novel automatic prognosis tool that can be helpful in early detection of the disease. 
Prediction model for diabetes  using Logistic Regression,Decision tree and Deep Learning.
Datasets used can be found at:
https://drive.google.com/drive/folders/19DF32fBPF6XfyOItmWDaj826oA-_cZNS?usp=sharing

*Remember to change the location of your dataset while performing.
Thankyou!

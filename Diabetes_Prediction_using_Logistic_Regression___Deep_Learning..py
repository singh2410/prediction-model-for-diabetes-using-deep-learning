#!/usr/bin/env python
# coding: utf-8

# #Prediction of Diabetes from available datasets using Logistic Regression Algo & Deep Learning.
# #By- Aarush Kumar
# #Dated- May 11,2021

# In[12]:


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


# In[4]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Diabetes Prediction/diabetes.csv')


# In[5]:


df


# In[6]:


df.head()


# In[7]:


df.shape


# In[8]:


df.info()


# In[10]:


df.isnull().sum()


# In[11]:


df.describe()


# In[16]:


df['Outcome'].value_counts()


# In[18]:


df.boxplot(figsize=(13,7))
plt.show()


# In[19]:


plt.figure(figsize=(13,7))
sns.boxplot(data=df,orient='h')
plt.show()


# In[21]:


plt.figure(figsize=(13,7))
sns.scatterplot(x=df['Insulin'],y=df['Outcome'])
plt.show()


# #Replacing 0 with respective column.

# In[27]:


X=df.drop('Outcome',axis=1)


# In[28]:


Y=df['Outcome']


# In[29]:


X


# In[30]:


Y


# In[31]:


X.describe()


# In[32]:


X.replace(to_replace=0,value=X.mean(),inplace=True)


# In[33]:


X.describe()


# In[34]:


X.boxplot(figsize=(13,7))
plt.show()


# In[39]:


Y.plot(figsize=(13,7))
plt.show()


# Splitting data for training & testing.

# In[40]:


X


# In[41]:


Y


# In[42]:


from sklearn.model_selection import train_test_split


# In[43]:


X_train , X_test , Y_train , Y_test = train_test_split(X,Y,test_size=0.25,random_state=None)


# In[44]:


X_train


# In[45]:


Y_train


# In[46]:


Y_train.value_counts()


# Standard Scalar

# In[47]:


from sklearn.preprocessing import StandardScaler
std=StandardScaler()


# In[48]:


X_train_std=std.fit_transform(X_train)
X_test_std=std.transform(X_test)


# In[49]:


X_train_std


# Training of model(Logistic Regression)

# In[50]:


from sklearn.linear_model import LogisticRegression
lr=LogisticRegression()


# In[51]:


lr.fit(X_train_std,Y_train)


# In[52]:


Y_pred=lr.predict(X_test_std)


# In[53]:


Y_pred


# In[54]:


Y_test


# In[55]:


from sklearn.metrics import accuracy_score


# In[56]:


accuracy_score(Y_test,Y_pred) 


# In[57]:


accuracy_score(Y_test,Y_pred) * 100


# Decision Tree

# In[58]:


from sklearn.tree import DecisionTreeClassifier
dt=DecisionTreeClassifier()


# In[59]:


dt.fit(X_train_std,Y_train)


# In[60]:


Y_pred=dt.predict(X_test_std)


# In[61]:


Y_pred


# In[62]:


Y_test


# In[63]:


accuracy_score(Y_test,Y_pred)


# In[64]:


accuracy_score(Y_test,Y_pred)*100


# Deep Learning

# In[65]:


from sklearn.neural_network import MLPClassifier
mlp=MLPClassifier(hidden_layer_sizes=(8,8))


# In[66]:


mlp.fit(X_train_std,Y_train)


# In[67]:


Y_pred=mlp.predict(X_test_std)


# In[68]:


accuracy_score(Y_test,Y_pred)


# In[69]:


accuracy_score(Y_test,Y_pred)*100

